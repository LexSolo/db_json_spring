package com.lex.jsonpostgres.repositories;

import com.lex.jsonpostgres.entities.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}

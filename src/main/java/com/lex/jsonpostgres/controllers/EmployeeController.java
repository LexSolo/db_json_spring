package com.lex.jsonpostgres.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lex.jsonpostgres.entities.Employee;
import com.lex.jsonpostgres.repositories.EmployeeRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class EmployeeController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(EmployeeController.class);

    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @RequestMapping("json")
    public void json() {
        // get json from resources
        // для windows
        /*
        URL url = this.getClass().getClassLoader().getResource("employees.json");
        
        if (url != null) {
            File jsonFile = new File(url.getFile());

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                List<Employee> employees = objectMapper.readValue(jsonFile, new TypeReference<List<Employee>>(){
                });

                employeeRepository.saveAll(employees);

                logger.info("All records are saved.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else logger.warning("URL is null.");
         */
        File jsonFile = null;
        try {
            jsonFile = ResourceUtils.getFile("classpath:employees.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            List<Employee> employees = objectMapper.readValue(jsonFile, new TypeReference<List<Employee>>(){
            });

            employeeRepository.saveAll(employees);

            logger.info("All records are saved.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
